DROP TABLE IF EXISTS cars;
DROP TABLE IF EXISTS  company;
DROP TRIGGER IF EXISTS add_price_UAH_collumn ON cars;

CREATE TABLE IF NOT EXISTS cars  (name varchar(100), price_USD float, price_UAH float, model varchar(50), year int);
INSERT INTO cars (name, price_USD, model, year) values ('i30', 15000, 'hyundai', 2019),
('i20', 12000, 'hyundai', 2018),
('Tucson', 20000, 'hyundai', 2013),
('Accent', 18000, 'hyundai', 2020); 


CREATE TABLE company (name varchar(50) PRIMARY KEY unique, country varchar(100), year_est int);
INSERT INTO company (name, country, year_est) values ('hyundai', 'South Korea', 1967),  ('Toyota', 'Japan', 1937);


CREATE OR REPLACE FUNCTION add_price_UAH( ) RETURNS TRIGGER AS $$
BEGIN
    NEW.price_UAH = NEW.price_USD * 26.84;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER add_price_UAH_collumn BEFORE INSERT OR UPDATE
    ON cars
    FOR EACH ROW
    EXECUTE PROCEDURE add_price_UAH();


INSERT INTO cars (name, price_USD, model, year) values ('Zaporozhets', 1000, 'ZAZ-968', 1971),
('Camry', 24000, 'Toyota', 2017),
('RAV4', 22000, 'Toyota', 2015),
('santa fe', 20000, 'hyundai', 2018),
('Land Cruiser', 30000, 'Toyota', 2014);

SELECT model, price_USD, price_UAH FROM cars;
